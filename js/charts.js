Highcharts.setOptions({
    lang: {
        decimalPoint: "," ,
        }
    });

Highcharts.chart('rada1', {
    chart: {
        type: 'line'
    },
    title: {
        text: 'Příležitostní a každodenní kuřáci'
    },
    subtitle: {
        text: 'zdroj: <a href="http://www.who.int/gho/tobacco/use/en/">odhad WHO</a>'
    },
    credits: {
        enabled: false
    },       
    xAxis: {
        categories: ['2000', '2005', '2010', '2015']
    },
    yAxis: {
        title: {
            text: 'Podíl kuřáků'
        },
        labels: {
            formatter: function() {
                return this.value+" %";
            }
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: false
        }
    },
    series: [{
        name: 'kuřáci celkem',
        data: [33.8, 32.7, 31.8, 30.8]
    }, {
        name: 'každodenní kuřáci',
        data: [25.2, 24.3, 23.6, 22.9]
    }]
});

Highcharts.chart('rada2', {
    chart: {
        type: 'line'
    },
    title: {
        text: 'Muži a ženy mezi kuřáky'
    },
    subtitle: {
        text: 'zdroj: <a href="http://www.who.int/gho/tobacco/use/en/">odhad WHO</a>'
    },
    credits: {
        enabled: false
    },       
    xAxis: {
        categories: ['2000', '2005', '2010', '2015']
    },
    yAxis: {
        title: {
            text: 'Podíl kuřáků'
        },
        labels: {
            formatter: function() {return this.value+" %";}
        }        
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: false
        }
    },
    series: [{
        name: 'muži',
        data: [40.3, 38.8, 37.4, 35.9]
    }, {
        name: 'ženy',
        data: [33.8, 32.7, 31.8, 30.8]
    }]
});

Highcharts.chart('pocet', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Počet vykouřených cigaret'
        },
        credits: {
            enabled: false
        },   
        subtitle: {
            text: 'Roční průměr na jednoho Čecha'
        },
        xAxis: {
            categories: ['2001','2002','2003','2004','2005','2006','2007','2008','2009','2010','2011','2012','2013','2014','2015'],
            crosshair: true
        },
        yAxis: {
            min: 0,
        },
        plotOptions: {
            bar: {
                pointPadding: 0.05,
                borderWidth: 0,
                enableMouseTracking: false,
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
                enabled: false
        },
        series: [{
            name: 'Cigarety',
            data: [1663,1893,2192,2243,2275,2338,2345,2107,2071,2028,1988,1947,1904,1950,2010]

        }]
});

Highcharts.chart('vek', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Kuřáci podle věku'
        },
        credits: {
            enabled: false
        },   
        subtitle: {
            text: 'zdroj: <a href="http://www.szu.cz/uploads/documents/czzp/zavislosti/Uzivani_tabaku_2015.pdf">Státní zdravotnický ústav</a>'
        },
        xAxis: {
            categories: ['15-24','25-44','45-64','65+'],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {text: 'Podíl kuřáků',},
            labels: {
            formatter: function() {
                return this.value+" %";
            }}
        },
        plotOptions: {
            column: {
                pointPadding: 0.05,
                borderWidth: 0,
                enableMouseTracking: false,
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
                enabled: false
        },
        series: [{
            name: 'Cigarety',
            data: [35.3,27.4,23.1,11.8]

        }]
});

Highcharts.chart('vzdelani', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Kuřáci podle vzdělání'
        },
        credits: {
            enabled: false
        },   
        subtitle: {
            text: 'zdroj: <a href="http://www.szu.cz/uploads/documents/czzp/zavislosti/Uzivani_tabaku_2015.pdf">Státní zdravotnický ústav</a>'
        },
        xAxis: {
            categories: ['ZŠ a SOU','SŠ s maturitou','VŠ'],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {text: 'Podíl kuřáků',},
            labels: {
            formatter: function() {
                return this.value+" %";
            }}            
        },
        plotOptions: {
            column: {
                pointPadding: 0.05,
                borderWidth: 0,
                enableMouseTracking: false,
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
                enabled: false
        },
        series: [{
            name: 'Cigarety',
            data: [24.2,24.3,14.3]

        }]
});