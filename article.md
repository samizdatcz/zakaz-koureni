title: "Data: Zákaz kouření postihne až třetinu Čechů. Na jednoho připadají dva tisíce cigaret ročně"
perex: "<small>Podle nejnovějších průzkumů kouří v Česku přes třicet procent lidí, nejčastěji mezi patnácti a čtyřiadvaceti lety. Podíl kuřáků za posledních 15 let klesl jen nepatrně.</small>"
authors: ["Michal Zlatkovský"]
published: "31. května 2017"
coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
coverimg_note: "Foto <a href='#'>ČTK</a>"
styles: []
libraries: ["https://unpkg.com/jquery@3.2.1", "http://code.highcharts.com/highcharts.js"]
options: "" #wide
---

Dnes, na Světový den bez tabáku, začíná platit takzvaný protikuřácký zákon, který zakazuje kouření mimo jiné ve všech restauracích a kavárnách. Dotkne se více než třech miliónů českých kuřáků a kuřaček. Podle dat ze zemí, které už kouření ve veřejném prostoru omezily v minulosti, to však jejich spotřebu cigaret nejspíš neomezí.

<div id="rada1"></div>

Vyplývá to z [průzkumu agentury Euromonitor](http://www.ceskatelevize.cz/ct24/ekonomika/2116425-az-zacne-platit-zakaz-koureni-trh-s-cigaretami-se-propadne-ale-kuraci-se-vrati), podle kterého se sice počet prodaných cigaret v roce zákazu kouření v restauracích ve většině zemí snížil, do dvou let se ale vrátil k původnímu vývoji. Patří mezi ně Irsko, Francie nebo Španělsko.

V Česku se i přes postupnou změnu společenské atmosféry celkový počet kuřáků mění jen málo. Mezi lety 2000 a 2015 se počet kuřáků zmenšil asi o pět procent, kuřaček pak o tři procenta.

<div id="rada2"></div>

Roční počet vykouřených cigaret se v roce 2015 oproti předchozím letům dokonce mírně zvýšil. Na každého Čecha připadlo předloni 2010 cigaret, tedy přes sto běžných krabiček ročně a asi pět a půl cigarety každý den. Číslo navyšuje vysoký počet denně kouřících Čechů - těch je téměř 23 procent. 

<div id="pocet"></div>

Mezi českými kuřáky jsou nejsilnější mladší ročníky. Ve věkové skupině 15 až 24 let kouří přes 35 procent lidí. S rostoucím věkem pak podíl klesá, ve věku nad 65 let kouří jen zhruba každý desátý Čech.

<div id="vek"></div>